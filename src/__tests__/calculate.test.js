import React from 'react';
import calculate from "../logic/calculate";


describe('Calculate Tests', ()=>{
    test('Number button tests', () => {
        expect(calculate({
                previous: null,
                current: null,
                operation: null},"4")).toHaveProperty('current', "4");
        expect(calculate({
                previous: null,
                current: 1,
                operation: null},"1")).toHaveProperty('current', "11");
        expect(calculate({
                previous: null,
                current: null,
                operation: "+"},"1")).toHaveProperty('current', "1");
    });
    
    
    test('Decimal point button tests', () => {
        expect(calculate({
                previous: 1.1,
                current: null,
                operation: null},".")).toHaveProperty('current', "0.");
    });


    test('Decimal point button tests', () => {
        expect(calculate({
                previous: 2.1,
                current: null,
                operation: null},".")).toHaveProperty('current', "0.");
    });


    test('Decimal point button tests', () => {
        expect(calculate({
                previous: 3.1,
                current: null,
                operation: null},".")).toHaveProperty('current', "0.");
    });


    test('Decimal point button tests', () => {
        expect(calculate({
                previous: 4.1,
                current: null,
                operation: null},".")).toHaveProperty('current', "0.");

    });

    test('Addition button tests', () => {
        expect(calculate({
                previous: null,
                current: 1,
                operation: null},"+")).toHaveProperty('current', null);

    });

    test('Equal button tests', () => {
        expect(calculate({
                previous: 2,
                current: 2,
                operation: "+"},"=")).toHaveProperty('previous', "4");
        expect(calculate({
                previous: 2,
                current: 21,
                operation: "+"},"=")).toHaveProperty('previous', "23");
    });
    
    
    test('Percentage button test', () => {
    	expect(calculate({
                previous: null,
                current: 5,
                operation: "+"},"%")).toHaveProperty('current', null);
    });
    test('Percentage button test', () => {
        expect(calculate({
                previous: 5,
                current: 5,
                operation: "÷"},"%")).toHaveProperty('previous', "0.01");
    }); 
    test('Percentage button test', () => {
        expect(calculate({
                previous: 10,
                current: 5,
                operation: "-"},"%")).toHaveProperty('previous', "0.05");
    });     
    test('Percentage button test', () => {
        expect(calculate({
                previous: 6,
                current: 5,
                operation: "x"},"%")).toHaveProperty('previous', "0.3");
    });
    
    
    test('+/- button test1', () => {
        expect(calculate({
                previous: 88,
                current: 77,
                operation: null},"+/-")).toHaveProperty('current', "-77");        
    });
    test('+/- button test2', () => {
        expect(calculate({
                previous: null,
                current: 90,
                operation: null},"+/-")).toHaveProperty('current', "-90");        
    });
    test('+/- button test3', () => {
        expect(calculate({
                previous: 57,
                current:null ,
                operation: null},"+/-")).toHaveProperty('previous', "-57");      
    });
    test('+/- button test4', () => { 
        expect(calculate({
                previous: null,
                current: -59,
                operation: null},"+/-")).toHaveProperty('current', "59"); 
    });
    test('+/- button test5', () => { 
        expect(calculate({
                previous: null,
                current: null,
                operation: null},"+/-")).toHaveProperty('[]', {}); 
    });
});



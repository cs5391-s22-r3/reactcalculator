import React from 'react';
import operate from "../logic/operate";
import Big from "big.js";



describe('Operate Multiplication Tests', ()=>{
    test('4 x 3 = 12 test', () => {
        expect(operate("4","3","x")).toBe("12"); 
    });
    test('16.2 x 6 = 97.2 test', () => {
        expect(operate("16.2","6","x")).toBe("97.2"); 
    });
 

});
describe('Operate Multiplication Tests', ()=>{
    test('1.1111 x 6 = 6.6666 test', () => {
        expect(operate("1.1111","6","x")).toBe("6.6666"); 
    });
    test('1.0011 x 8 = 8.0088 test', () => {
        expect(operate("1.0011","8","x")).toBe("8.0088"); 
    });


});

describe('Operate Division Tests', ()=>{
    test('4/3 = 1.333 test', () => {
        expect(parseFloat(operate("4","3","÷"))).
            toBeCloseTo(1.3333333333, 5); 
        // parseFloat() is a string-to-float function in Javascript.   
        // Try different precisions and string lengths to see what the test does.
    });
    test('20/5 = 4 test', () => {
        expect(operate("20","5","÷")).toBe("4"); 
    });
    
});

describe('Operate Division Tests', ()=>{
test('41/3 = 13.6666 test', () => {
    expect(parseFloat(operate("41","3","÷"))).
        toBeCloseTo(13.6666666, 6); 
    // parseFloat() is a string-to-float function in Javascript.   
    // Try different precisions and string lengths to see what the test does.
});
test('16/3 = 5.3 test', () => {
    expect(parseFloat(operate("16","3","÷"))).
    toBeCloseTo(5.33333, 5); 
});
describe('Operate Addition Tests', ()=>{
    test('-15 + 7 = -8 test', () => {
        expect(operate("-15","7","+")).toBe("-8");
    });
    test('34 + (-76) = -42 test', () => {
        expect(operate("34","-76","+")).toBe("-42");
    });
});

describe('Operate Substraction Tests', ()=>{
    test('26 - 98 = -72 test', () => {
        expect(operate("26","98","-")).toBe("-72");
    });
    test('-67 - 102 = -35 test', () => {
        expect(operate("-67","102","-")).toBe("-169");
    });
});
});


